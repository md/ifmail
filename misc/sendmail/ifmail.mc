divert(-1)
include(`../m4/cf.m4')
OSTYPE(`linux')
dnl ##########################
dnl #  Configurable options  #
dnl ##########################

dnl ### Your ISP smtp host (leave blank if none)
dnl ### the brackets ( [ ] ) avoid calling the DNS; usefull if you
dnl ### are not connected to the internet 24h/24
define(`SMART_HOST',		``[smtp.your.provider.com]'')

dnl ### The fidonet address of your uplink; that is the default ftn route
define(`FIDO_SMART_HOST',	``f1.n2.z3.fidonet.org'')

dnl ### The fidonet --> internet gateway
define(`FIDO_GATEWAY',		``f4.n5.z6.fidonet.org'')

dnl ### define or undefine depending if you want to use the gateway or
dnl ### not (if you ave a real email access undefine it)
define(`USE_FGATE')

dnl ### This is needed if you don't have permanent access to DNS
dnl ### (that is if you are not 24h/24 wired; if you are you can remove it)
FEATURE(accept_unresolvable_domains)
FEATURE(nodns)dnl

dnl
dnl ####################################
dnl #    End of configurable section   #
dnl ####################################
dnl

define(`confDEF_USER_ID',``8:12'')
define(`confMATCH_GECOS',`True')
define(`confTRY_NULL_MX_LIST',`True')
define(`confTO_QUEUEWARN', `2d')
define(`confTO_QUEUERETURN', `8d')
define(`confUSE_ERRORS_TO',`True')
define(`confTRUSTED_USERS',`fnet')
define(`confCT_FILE', ` -o /etc/mail/sendmail.ct')dnl
define(`confCW_FILE', ` /etc/mail/sendmail.cw')dnl
define(`confPRIVACY_FLAGS', `authwarnings,noexpn,novrfy')dnl
define(`confSTATUS_FILE',`/var/run/sendmail.st')dnl
define(`ALIAS_FILE',`/etc/mail/aliases,/etc/mail/majordomo')dnl
define(`HELP_FILE',`/etc/mail/sendmail.hf')dnl
define(`PROCMAIL_MAILER_PATH',`/usr/bin/procmail')dnl
define(`STATUS_FILE',`/var/run/sendmail.st')dnl
undefine(`UUCP_RELAY')dnl
undefine(`BITNET_RELAY')dnl
FEATURE(access_db, hash -o /etc/mail/access)dnl
FEATURE(always_add_domain)dnl
FEATURE(blacklist_recipients)dnl
dnl FEATURE(limited_masquerade)dnl
dnl FEATURE(masquerade_entire_domain)dnl
FEATURE(masquerade_envelope)dnl
FEATURE(local_procmail)dnl
FEATURE(redirect)dnl
FEATURE(relay_based_on_MX)dnl
FEATURE(relay_entire_domain)dnl
FEATURE(relay_local_from)dnl
FEATURE(use_ct_file)dnl
FEATURE(use_cw_file)dnl
FEATURE(`domaintable',`hash -o /etc/mail/domaintable')dnl
FEATURE(`genericstable',`hash -o /etc/mail/genericstable')dnl
GENERICS_DOMAIN_FILE(confCW_FILE)dnl
FEATURE(`mailertable',`hash -o /etc/mail/mailertable')dnl
FEATURE(`virtusertable',`hash -o /etc/mail/virtusertable')dnl
FEATURE(nocanonify)dnl
MAILER(procmail)dnl
MAILER(smtp)dnl
MAILER(fnet)dnl
MAILER(usenet)dnl
MAILER(uucp)dnl

LOCAL_CONFIG
# Pseudo-domains (don't call the DNS on them)
CPz1.fidonet.org z2.fidonet.org z3.fidonet.org z4.fidonet.org
CPz5.fidonet.org z6.fidonet.org ftn

# for fidonet address, don't send trough fnet mailer addresses like
# smtp.z2.fidonet.org, www.z2.fidonet.org, etc
CFfidonet ns ns2 mail smtp www ftp

# email address on other domains that we redirect
Kpirateo hash -o /etc/mail/pirateo

LOCAL_RULE_0
# pirateo
R$+ < @ $+ . > $*		$: < $(pirateo $1 @ $2 $: $) > $1 < @ $2 . > $3
R$+ < @ $+ $~. > $*		$: < $(pirateo $1 @ $2 $3 $: $) > $1 < @ $2 $3 > $4
R< $+ > $+ < @ $+ > $*		$@ $>97 $1 $4
R<> $+ < @ $+ > $*		$: $1 < @ $2 > $3

LOCAL_NET_CONFIG
# ************ FIDONET.ORG ***********
# for nodes allways put leading $* if you want to route his points too
# routed trough default smart host FIDO_SMART_HOST
R$* < @ $~F $+ .z1.fidonet.org . > $*	$#fnet $@ FIDO_SMART_HOST $: $1 < @ $2 $3 .z1.fidonet.org > $4
R$* < @ $~F $+ .z2.fidonet.org . > $*	$#fnet $@ FIDO_SMART_HOST $: $1 < @ $2 $3 .z2.fidonet.org > $4
R$* < @ $~F $+ .z3.fidonet.org . > $*	$#fnet $@ FIDO_SMART_HOST $: $1 < @ $2 $3 .z3.fidonet.org > $4
R$* < @ $~F $+ .z4.fidonet.org . > $*	$#fnet $@ FIDO_SMART_HOST $: $1 < @ $2 $3 .z4.fidonet.org > $4
R$* < @ $~F $+ .z5.fidonet.org . > $*	$#fnet $@ FIDO_SMART_HOST $: $1 < @ $2 $3 .z5.fidonet.org > $4
R$* < @ $~F $+ .z6.fidonet.org . > $*	$#fnet $@ FIDO_SMART_HOST $: $1 < @ $2 $3 .z6.fidonet.org > $4

# all the remaining FTN's (via fido smart host FIDO_SMART_HOST)
R$* < @ $+ .ftn . > $*			$#fnet $@ FIDO_SMART_HOST $: $1 < @ $2 .ftn > $3

# If you don't have internet connectivity comment out this line
# to send through the gateway FIDO_GATEWAY
# (packets will go to your default uplink FIDO_SMART_HOST)
ifdef(`USE_FGATE',`',`#')R$* < @ $* > $*			$#fnet $@ FIDO_SMART_HOST $: $1 % $2 < @ FIDO_GATEWAY > $3

