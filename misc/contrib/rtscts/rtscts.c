/* rtscts.c -- Sets the RTS/CTS flow control mode for RISCos
 * Marc SCHAEFER <schaefer@alphanet.ch>
 * V1.0 PV002 MSC95
 * 15/01/95
 * DESCRIPTION
 *    This program will set the RTS/CTS mode bit on the io file
 *    correponding to stdin (file handle 0).
 * NOTE
 *    Must be called by any RISCos SVR4 program wanting to use RTS/CTS flow
 *    control while dialing out (RISCos SVR4 is broken in this aspect)
 */

#define STRANGE

#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <errno.h>
#include <termio.h>

int main(argc, argv)
int argc;
char **argv;
{
   struct termio arg;

#ifndef STRANGE
   if (argc != 1) {
      fprintf(stderr, "%s\n", argv[0]);
      fprintf(stderr, "%s: bad args\n", argv[0]);
      exit(1);
   }
#endif
   ioctl(0, TCGETA, &arg);
   
   arg.c_cflag |= CNEW_RTSCTS;

   ioctl(0, TCSETA, &arg);
   exit(0);
}
