 Conversion of areatags to newsgroups (with distribution) and back.

 Lines starting with a whitespace are comments.  This is because
 areatags may contain a hash ('#') character which would otherwise
 be sutable as a comment designator.

 AREATAG   newsgroup   distribution   modtype:moderator

 If you compile with -DJE, the format is:
 AREATAG  newsgroup   distribution   rfc-charset  FTN-CHRS   modtype:moderator

 "rfc-charset" is the charset used in the rfc (usenet/email) side. It must
 be in the format used in MIME headers (ex: iso-8859-1, koi8-r, EUC-kr,...)
 "FTN-CHRS" is the charset used in the FTN side. It must be in the format
 used in ^aCHRS: kludges, whitout de level number (ex: LATIN-1, KOI8, EUC-KR)
 See file README.charset for a list of recognized rfc-charset and FTN-CHRS

 "moderator" is an rfc address of the moderator.
 "modtype" is either "umod","fro" or "fstd".
 umod stands for usenet moderated, messages arriving in a pkt are converted
      to email for the defined moderator.
 fro  stands for fido read-only; when the gated area is defined as (Moderated)
      it create correct "Approved:" lines.
 fstd stands for fido standart way of moderation, (does nothing yet)

 exemple (with -DJE):

 COMERZ  fido.commerz world iso-8859-1 CP437 fro:Jan.Smith@f1.n2.z3.fidonet.org


 ### [ There is the list I use for belgian newsgroups ]
 ### [ I don't use Distribution field, I see no need to add one... ]
 ### [ only for a few fido-only distributed areas ]

 ########### be.*
BE.ANNOUNCE		be.announce  		
BE.COMMERCIAL		be.commercial 		
BE.COMP			be.comp   		
BE.FORSALE		be.forsale 		
BE.JOBS			be.jobs 		
BE.MISC			be.misc                		
BE.POLITICS		be.politics              		
BE.SCIENCE		be.science                 		
BE.TV			be.tv       		

 ########### fido.belg.*
3DSTUDIO.B		fido.belg.3dstudio		
AIDS.B			fido.belg.aids		
ALLFIX.B		fido.belg.allfix.general		
ALLFIX_SUP.B		fido.belg.allfix.support		
AMIGA.B			fido.belg.amiga		
ASTRONOM.B		fido.belg.astronomy		
AUTO.B			fido.belg.auto		
AVONTUUR.B		fido.belg.avontuur		
BABEL.B			fido.belg.babel		
BACKBONE.B		fido.belg.backbone     		
FAKEUSER.B		fido.belg.bbs.fake-users		
BBSADV.B		fido.belg.bbsadv       		
BEER.B			fido.belg.beer		
BEURS.B			fido.belg.beurs		
BEURS-DATA.B		fido.belg.beurs-data		
BIJBEL.B		fido.belg.bijbel		
BOEKEN.B		fido.belg.boeken		
CDROM.B			fido.belg.cdrom		
CLIPPER.B		fido.belg.clipper		
CLUBS.B			fido.belg.clubs		
COMICS.B		fido.belg.comics		
DBHULP.B		fido.belg.dbridge.support		
DVNET.B			fido.belg.dvnet		
ELEKTRON.B		fido.belg.electron		
FILES.B			fido.belg.files		

 ########### fido.belg.fra.*
ASM-TUTORIAL.BF		fido.belg.fra.asm-tutorial		
AUTOMOBILE.BF		fido.belg.fra.automobile		
C_ECHO.BF		fido.belg.fra.c		
CLIPPER.BF		fido.belg.fra.clipper		
COMMERCE.BF		fido.belg.fra.commerce		
COMMS.BF		fido.belg.fra.communications		
COMPNEWS.B		fido.belg.fra.compuserve.news		
ED-MSG.BF		fido.belg.fra.editeurs_msg		
FIDONET.BF		fido.belg.fra.fidonet		
FILE-SEARCH.B		fido.belg.fra.files.search		
FILE-SEARCH.B		fido.belg.fra.files.search		
FILM.BF			fido.belg.fra.film		
GENEALOGY.BF		fido.belg.fra.genealogy		
GENERAL.B		fido.belg.fra.general		
HARD.BF			fido.belg.fra.hardware		
INTERNET.BF		fido.belg.fra.internet		
ISDN.BF			fido.belg.fra.isdn		
LINUX.BF		fido.belg.fra.linux		
MAXIMUS.BF		fido.belg.fra.maximus		
OCCASES.BF		fido.belg.fra.occases		
OS2.BF			fido.belg.fra.os2		
RELIGION.BF		fido.belg.fra.religion		
RTFM			fido.belg.fra.rtfm		
SCIENCES.BF		fido.belg.fra.sciences		
SOFT.BF			fido.belg.fra.soft		
SONS.BF			fido.belg.fra.sons  		
SYSTEMES.BF		fido.belg.fra.systemen		
WIN.BF			fido.belg.fra.windows		

 ########### fido.belg.* (2)
FDHELP.B		fido.belg.frontdoor.support		
FRUSTRATIE.B		fido.belg.frustratie		
FSFAN.B			fido.belg.fsfan		
FUNPROG.B		fido.belg.funprog		
GAMING.B		fido.belg.gaming		
GOLDED.B		fido.belg.golded		
HAM.B			fido.belg.ham		
HOUSE.B			fido.belg.house		
HP48.B			fido.belg.hp48		
INTERNET.B		fido.belg.internet		
JOB.B			fido.belg.jobs		
KEUKEN.B		fido.belg.keuken		
KINDEREN.B		fido.belg.kinderen		
K.U.LEUVEN		fido.belg.kuleuven		
LAN.BF			fido.belg.lan
LASERDISC.B		fido.belg.laserdiscs		
LINUX.B			fido.belg.linux		
SIMENON.B		fido.belg.literature.simenon		
LOSTKIDS.B		fido.belg.lostkids		
LOTTO.B			fido.belg.lotto		
LUKRAAK.B		fido.belg.lukraak		
MACBEL.B		fido.belg.macbel		
MATH.B			fido.belg.math		
MAX.B			fido.belg.maximus		
MEDIA.B			fido.belg.media		
MODEL_MAKING.B		fido.belg.model_making		
MODEM.B			fido.belg.modem		
MODERATORS.B		fido.belg.moderators		
MUZIEK.B		fido.belg.music		
NATSYSOP.B		fido.belg.natsysop		fido

 ########### fido.belg.ned.*
ALGEMEEN.B		fido.belg.ned.algemeen		
ALLES.BN		fido.belg.ned.alles		
DIER.B			fido.belg.ned.animals		
DAMES.B			fido.belg.ned.computer.widows		
COMPUTER&RECHT		fido.belg.ned.computer_recht		
DWARZ.B			fido.belg.ned.dragon.warz		
FILM.B			fido.belg.ned.films		
GRAP.B			fido.belg.ned.grappen		
HANDEL.B		fido.belg.ned.handel		
FOTO.B			fido.belg.ned.photography		
PROBOARD.B		fido.belg.ned.proboard		
SYSTEMEN.BN		fido.belg.ned.systemen		
WINDOWS.BN		fido.belg.ned.windows		

 ########### fido.belg.* (3)
OS2.B			fido.belg.os2		
OS2COM.B		fido.belg.os2com		
OUWEZAK.B		fido.belg.ouwezak		
P-DOMAIN.B		fido.belg.p-domain		
PARAVISIE.B		fido.belg.paravisie		
PGP.B			fido.belg.pgp		
POINTS.B		fido.belg.points		
POLITIC.B		fido.belg.politics		
PROBOARD.029		fido.belg.proboard		
PROJECT.B		fido.belg.project		
QB.B			fido.belg.quickbasic		
RA_SUP.B		fido.belg.ra.support		
RAM.B			fido.belg.ram		
RAY_FRA_ANI.B		fido.belg.ray_fra_ani		
RECHT.B			fido.belg.recht		
RPG.B			fido.belg.rpg		
SATELLITE.B		fido.belg.satellite		
SEX.B			fido.belg.sex		
SJACHEL.B		fido.belg.sjachel		
SOUND_CORNER.B		fido.belg.sound_corner		
STUDENTEN.B		fido.belg.studenten		
TABOE			fido.belg.taboe		
RAIL.B			fido.belg.trains		
UFO.B			fido.belg.ufo		
UITGAAN.B		fido.belg.uitgaan		
UNIX.B			fido.belg.unix		
VIRTUAL.B		fido.belg.virtual_reality		
VIRUS.B			fido.belg.virus		
VUB			fido.belg.vub		
BNET.B			fido.belg.vzw.bnet		
IMPACT			fido.belg.vzw.impact		
WAPEN.B			fido.belg.wapen		
WELZIJN.B		fido.belg.welzijn		
WETENSCHAP.B		fido.belg.wetenschap		
ZYXEL.B			fido.belg.zyxel        		

  Line with a single '*' in the "newsgroup" field defines default areatag.
  '*' in the "AREA" field, if present, is expanded to the original
  newsgroup (converted to uppercase).  (Distribution is insignificant).
 
  If you define it, an FTN message will be generated for each newsgroup in
  the Newsgroups: header, even if you're not linked to it, and an ECHO doesn't
  really exist for it. Is better not to define.

  U_*                     *
 
  Line with a single '*' in the "AREA" field defines default newsgroup
  and distribution.  '*' in the "newsgroup" field, if present, is expanded
  to the original areatag (converted to lowcase).

*                       fido.*                  	fido

  You can specify a line with a single asterisk in the "AREA" field and
  a single asterisk in the "newsgroup" field.  It will cause a reversible
  conversion - areatag will be an uppercased newsgroup, and a newsgroup 
  will be a lowercased areatag.  This is not recommended.

  *                     *                       world
