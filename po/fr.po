# French file for ifmail-tx
msgid ""
msgstr ""
"Date: 1997-07-24 21:53:47+0200\n"
"Project-Id-Version: ifmail 2.13-tx\n"
"PO-Revision-Date: 1997-07-24 21:53:47+0200\n"
"From: root <srtxg@chanae.alphanet.ch>\n"
"Last-Translator: Pablo Saratxaga <srtxg@chanae.alphanet.ch>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-1\n"
"Content-Transfer-Encoding: 8-bit\n"
"Xgettext-Options: --default-domain=ifmail --add-comments --keyword=_ --keyword=N_\n"
"Files: ../iflib/rdconfig.c ../ifgate/iftoss.c ../ifgate/ifmail.c ../ifgate/ifpack.c ../ifcico/ifcico.c ../ifcico/ifroute.c\n"

#: ../iflib/rdconfig.c:471
msgid "%s ver. %s of %s; (c) %s\n"
msgstr "%s ver. %s du %s; (c) %s\n"

#: ../iflib/rdconfig.c:473
msgid ""
"    This is free software. You can do what you wish with it\n"
"    as long as this copyright notice is preserved.\n"
"\n"
msgstr ""
"    Ceci est un logiciel libre. Vous pouvez en faire ce que\n"
"    vous voulez tant que cette notice de copyright\n"
"    est conserv�e.\n"

#: ../iflib/rdconfig.c:475
msgid "usage: %s -h -x<N> -I<file> %s\n"
msgstr ""

#: ../iflib/rdconfig.c:476
msgid "-h\t\tget this help\n"
msgstr "-h\t\taffiche cette aide\n"

#: ../iflib/rdconfig.c:477
msgid "-x<arg>\t\tset debug level <arg>\t[%08lx]\n"
msgstr "-x<arg>\t\tplace le niveau de debug � <arg>\t[%08lx]\n"

#: ../iflib/rdconfig.c:479
msgid "\t\t<arg> may be a number from 0 to 32 to set `on'\n"
msgstr "\t\t<arg> peut �tre un nombre entre 0 et 32 pour activer\n"

#: ../iflib/rdconfig.c:480
msgid "\t\tbits from 1 to number, or a string of letters\n"
msgstr "\t\tles bits 1 jusqu'au nombre, ou une cha�ne de lettres\n"

#: ../iflib/rdconfig.c:481
msgid "\t\t('a' - bit 1, 'b' - bit 2, e.t.c. up to bit 26)\n"
msgstr "\t\t('a' - 1er bit, 'b' - 2e bit, etc. jusqu'au 26e bit)\n"

#: ../iflib/rdconfig.c:482
msgid "-I<file>\tuse config file\t<file>\t[%s]\n"
msgstr ""
"-I<file>\tsp�cifier le fichier\t<file>\t[%s]\n"
"\t\tcomme fichier de configuration � utiliser\n"

#: ../ifgate/iftoss.c:53 ../ifgate/iftoss.c:56
msgid "-N\t\tput messages to %s directory\n"
msgstr "-N\t\tcr�e les messages dans le r�pertoire %s\n"

#: ../ifgate/iftoss.c:57
msgid "-f\t\tforce tossing of packets addressed to other nodes\n"
msgstr "-f\t\tforce le tossage des paquets address�s � d'autres noeuds\n"

#: ../ifgate/ifmail.c:81
msgid "-N\t\tput packets to %s directory\n"
msgstr "-N\t\tcr�e les paquets dans le r�pertoire %s\n"

#: ../ifgate/ifmail.c:82
msgid "-o<flavors>\tforce `out' mode for these flavors\n"
msgstr "-o<flavors>\tforce le mode �out� pour ces saveurs\n"

#: ../ifgate/ifmail.c:83
msgid "-o+\t\tforce `out' mode for all flavors\n"
msgstr "-o+\t\tforce le mode �out� pour toutes les saveurs\n"

#: ../ifgate/ifmail.c:84
msgid "-o-\t\treset `out' mode for all flavors\n"
msgstr "-o-\t\treset du  mode �out� pour toutes les saveurs\n"

#: ../ifgate/ifmail.c:85
msgid "-n\t\tset news mode\n"
msgstr "-n\t\tmode �news�\n"

#: ../ifgate/ifmail.c:86
msgid "-s\t\trun in secure mode (check nodelist)\n"
msgstr "-s\t\tmode s�curis� (verification dans la nodeliste)\n"

#: ../ifgate/ifmail.c:87
msgid "-r<addr>\taddress to route packet\n"
msgstr "-r<addr>\tadresse par o� router le paquet\n"

#: ../ifgate/ifmail.c:88
msgid "-g<grade>\t[ n | c | h ] \"flavor\" of packet\n"
msgstr "-g<grade>\t[ n | c | h ] �saveur� du paquet\n"

#: ../ifgate/ifmail.c:90
msgid "-c<charset>\tforce the given charset\n"
msgstr "-c<charset>\tforce le charset sp�cifi�\n"

#: ../ifgate/ifmail.c:92
msgid "-l<level>\tforce the given level (default=%d)\n"
msgstr "-l<level>\tforce le niveau sp�cifi� (d�faut==%d)\n"

#: ../ifgate/ifmail.c:93
msgid "-b\t\tdon't split the messages\n"
msgstr "-b\t\tne pas saucissoner les messages\n"

#: ../ifgate/ifmail.c:94
msgid "<recip>\t\tlist of receipient addresses\n"
msgstr "<recip>\t\tliste des adresses des destinataires\n"

#: ../ifgate/ifpack.c:57
msgid "-N\t\tprocess %s directory\n"
msgstr "-N\t\tscane l'outbound %s\n"

#: ../ifgate/ifpack.c:58
msgid "-f\t\tpack *.?ut files too\n"
msgstr "-f\t\tcompresse les fichiers *.?ut aussi\n"

#: ../ifcico/ifcico.c:56
msgid "-j<num>\t\tdamage every <num> byte\t[%d]\n"
msgstr ""

#: ../ifcico/ifcico.c:65
msgid "-r 0|1\t\t1 - master, 0 - slave\t[0]\n"
msgstr "-r 0|1\t\t1 - ma�tre, 0 - esclave\t[0]\n"

#: ../ifcico/ifcico.c:66
msgid "-n<phone>\tforced phone number\n"
msgstr "-n<phone>\tforce un num�ro de t�l�phone\n"

#: ../ifcico/ifcico.c:67
msgid "-l<ttydevice>\tforced tty device\n"
msgstr "-l<ttydevice>\tforce un device tty\n"

#: ../ifcico/ifcico.c:69
msgid "-a<inetaddr>\tuse TCP/IP instead of modem\n"
msgstr "-a<inetaddr>\tutilise TCP/IP au lieu du modem\n"

#: ../ifcico/ifcico.c:70
msgid "-t<mode>\t0 - IFC, 1 - telnet\t[0]\n"
msgstr ""

#: ../ifcico/ifcico.c:72
msgid "  <node>\tshould be in domain form, e.g. f11.n22.z3\n"
msgstr "  <node>\tdoit �tre en format rfc, ex: f11.n22.z3\n"

#: ../ifcico/ifcico.c:73
msgid "\t\t(this implies master mode)\n"
msgstr "\t\t(ceci implique le mode �ma�tre�)\n"

#: ../ifcico/ifcico.c:74
msgid ""
"\n"
" or: %s tsync|yoohoo|**EMSI_INQC816\n"
msgstr "\n ou: %s tsync|yoohoo|**EMSI_INQC816\n"

#: ../ifcico/ifcico.c:75
msgid "\t\t(this implies slave mode)\n"
msgstr "\t\t((ceci implique le mode �esclave�)\n"

#: ../ifcico/ifroute.c:27
msgid "-d<domain>\tspecify top level domain for routing\n"
msgstr "-d<domain>\tsp�cifie le domaine � utiliser pour le routage\n"

#: ../ifcico/ifroute.c:28
msgid "-f<boss>\tspecify fallback node\n"
msgstr "-f<boss>\tsp�cifie l'uplink par d�faut\n"

#: ../ifcico/ifroute.c:29
msgid "-n\t\tdo not use nodelist when MX ok\n"
msgstr "-n\t\tne pas utiliser la nodeliste si les MX sont ok.\n"

#: ../ifcico/ifroute.c:30
msgid "  <node>\tin domain form, e.g. f11.n22.z3\n"
msgstr "  <node>\tdoit �tre en format rfc, ex: f11.n22.z3\n"
