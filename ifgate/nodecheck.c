#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <sys/stat.h>
#ifdef HAS_NDBM_H
#include <fcntl.h>
#include <ndbm.h>
#else
#include <dbm.h>
#endif
#include "directory.h"

#include "xutil.h"
#include "lutil.h"
#include "ftn.h"
#include "config.h"
#include "nodecheck.h"
#include "nlindex.h"
#include "needed.h"


int chknlent(addr)
faddr *addr;
{
	struct _ixentry xaddr;
	datum key;
	datum dat;
	int n;

	debug(20,"chknlent for %s",ascfnode(addr,0x1f));

	key.dptr=(char*)&xaddr;
	key.dsize=sizeof(struct _ixentry);

	if (addr == NULL) return(0);

	xaddr.zone=addr->zone;
	xaddr.net=addr->net;
	xaddr.node=addr->node;
	xaddr.point=0; /* We only check if the node/boss exist */

	switch (initnl())
	{
	case 0:	break;
	case 1:	loginf("WARNING: nodelist index needs to be rebuilt with \"ifindex\"");
		break;
	default: return(0);
	}

#ifdef HAS_NDBM_H
	dat=dbm_fetch(nldb,key);
#else
	dat=fetch(key);
#endif
	if (dat.dptr == NULL) return(0);
	n=dat.dsize/sizeof(struct _loc);
	debug(20,"found %d entries",n);
	return(1);
}
