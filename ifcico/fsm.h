#ifndef FSM_H
#define FSM_H

/*
   Finite State Machine.  Replace oldish "statetbl.h", use '#'
   preprocessor feature, detach state machine from function definition.
   Although, multi-machine (with more than one state variable)
   not implemented, so bidirectional protos will not use this.
*/

#define SM_STATES(name) \
	int sm_success=0;\
	char *sm_name=name;
	enum

#define SM_START(x) \
	sm_state=x;\
	debug(14,"statemachine %s start %s (%d)",sm_name,#x,x);\
	while (!sm_success) switch (sm_state)\
	{\
	default: logerr("statemachine %s error: state=%d",sm_name,sm_state);\
		sm_success=-1;

#define SM_STATE(x) \
	break;\
	case x: debug(15,"statemachine %s entering %s (%d)",\
		sm_name,#x,x);

#define SM_END \
	}\
	debug(14,"statemachine %s exit %s (%d)",\
		sm_name,(sm_success == -1)?"error":"success",sm_success);

#define SM_RETURN \
	return (sm_success != 1);\
}

#define SM_PROCEED(x) \
	sm_state=x; break;

#define SM_SUCCESS \
	sm_success=1; break;

#define SM_ERROR \
	sm_success=-1; break;

#endif
