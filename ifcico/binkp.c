#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <fcntl.h>
#include <time.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include "lutil.h"
#include "xutil.h"
#include "ttyio.h"
#include "session.h"
#include "statetbl.h"
#include "binkp.h"
#include "config.h"
#include "version.h"

extern void rdoptions(node *);
extern int nodelock(faddr*);
extern FILE *openfile(char*,time_t,off_t,off_t*,int(*)(off_t));
extern int closefile(int);
extern file_list *respond_wazoo(char*);

void binkp_send_control(int id,...);
static int resync(off_t off);

/* static char *mon[] = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
file_list *tosend=NULL;
static FILE *infile=NULL,*outfile=NULL;
long infilesize,infiletime,infileoffset;
long outfilesize,outfiletime,outfileoffset;
char *infilename,*outfilename;
static long instartime,outstartime,endtime,rxbytes,sbytes; */

int binkp(int mode)
{
	int rc=0;

	if (mode == 1) {
		loginf("start outbound BINKP session");
		loginf("Not implemented yet");
	} else {
		loginf("start inbound BINKP session");
		loginf("Not implemented yet");
	}

	binkp_send_control(M_ERR,"Binkp protocol not yet available");
	rc=15;

	rc=abs(rc);
	return rc;
}

void binkp_send_control(int id,...)
{
	va_list args;
	char *fmt, *s;
	binkp_frame frame;
	static char buf[1024];
	int i,sz;

	va_start(args,id);
	fmt=va_arg(args, char*);

	if (fmt) {
		vsprintf(buf,fmt,args);
		sz=((1+strlen(buf)) & 0x7fff);
	} else {
		buf[0]='\0';
		sz=1;
	}

	frame.header=((BINKP_CONTROL_BLOCK + sz) & 0xffff);
	frame.id=(char)id;
	frame.data=buf;

	s=(unsigned char *)malloc(sz+2+1);
	s[sz+2]='\0';
	s[0]=((frame.header >> 8)&0xff);
	s[1]=(frame.header & 0xff);
	s[2]=frame.id;
	if (frame.data[0]) strncpy(s+3,frame.data,sz-1);
	
	for (i=0; i<sz+2; i++)
		PUTCHAR(s[i]);

	va_end(args);
}

int resync(off_t off)
{
  return 0;
}

